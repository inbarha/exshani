import { BrowserModule } from '@angular/platform-browser';//מובנה
import { NgModule } from '@angular/core';//מובנה

import { AppComponent } from './app.component';//מובנה
import { ListComponent } from './list/list.component';//מובנה
import { ProductComponent } from './product/product.component';//מובנה

import {MatTableModule} from '@angular/material/table';//מטירייל-טבלה

import { AngularFireModule } from '@angular/fire';//פיירבייס
import { AngularFireDatabaseModule } from '@angular/fire/database';//פיירבייס
import { AngularFireAuthModule } from '@angular/fire/auth';//פיירבייס
import { environment } from '../environments/environment'; //פיירבייס


import { RouterModule, Routes } from '@angular/router';//ראוטר

import {MatCardModule} from '@angular/material/card';//לנאב
import {MatInputModule} from '@angular/material/input';//לטופס התחברות
import{FormsModule} from '@angular/forms'; //לטופס
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';//לטוס- אבל להוסיף תמיד
import {MatDialogModule} from '@angular/material/dialog';//פופאפ
import {MatFormFieldModule} from '@angular/material/form-field';//פופאפ

import {MatButtonModule} from '@angular/material/button';//כפתורים
import { MatGridListModule } from '@angular/material/grid-list';//טבלה

import { MatSelectModule } from '@angular/material/select';//לסינון

import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { app } from 'firebase';
import { EditComponent } from './edit/edit.component'; 




@NgModule({
  declarations: [
    NavComponent,
    AppComponent,//מובנה
    ListComponent,//מובנה
    ProductComponent, LoginComponent, HomeComponent, RegisterComponent,//מובנה
    EditComponent, // איפה שהפופ אפ


  ],
  imports: [
    BrowserModule,//מובנה
    MatTableModule, //מטירייל- טבלה
    AngularFireModule.initializeApp(environment.firebase), //פיירבייס
    AngularFireDatabaseModule,//פיירבייס
    AngularFireAuthModule,//פיירבייס
    MatCardModule, //לנאב
    MatInputModule,//להתחברות
    FormsModule,//לטופס
    BrowserAnimationsModule, //לטופס אבל להוסיף תמיד
    MatDialogModule,//פופאפ
    MatFormFieldModule,//פופאפ
    MatButtonModule,//כפתורים
    MatGridListModule,
    MatSelectModule,//סינון
   

    RouterModule.forRoot([
      {path:'',component:HomeComponent}, //מכיל יו-אר-אל אם הוא ריק מדובר באינדקס בקומפוננט נרשום את הקומפוננטים שמשתנים
      {path:'list',component:ListComponent},
      {path:'home',component:HomeComponent},
      {path:'login',component:LoginComponent},      
      {path:'register',component:RegisterComponent},
      {path:'**',component:HomeComponent},
    
      ]),
  ],
  providers: [],//מובנה
  bootstrap: [AppComponent],//מובנה
  entryComponents: [EditComponent]//איפה שהפופאפ
})
export class AppModule { }
